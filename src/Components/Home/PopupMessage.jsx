import { useEffect, useState } from "react";


export default function PopupMessage({Ubicacion}){

    const [permisoUbicacion, setPermisoUbicacion] = useState(false);
    const [posicion, setPosicion] = useState({
        latitud: 0,
        longitud: 0
    });

    const [error, setError] = useState("");
    
    useEffect(() => {

        // chrome://flags
        // secure origin Enable y Poner la Ip

        if(navigator.geolocation){

            if(navigator.geolocation.getCurrentPosition((position) => {
              
                setPosicion({
                    latitud: position.coords.latitude,
                    longitud: position.coords.longitude
                });
    
            }, (e) => {
                
                console.log(e);
                setError(e.message);
                setPosicion({
                    latitud: 0,
                    longitud: 0
                });
    
            }));

        }
        
    }, []);

    return(
        <div>
            <p>
                <b>🌽 Rincon del Snack 🌽</b><br />
                {error}
            </p>
            <p>
                <a href={'https://www.google.com.mx/maps/dir/'+ posicion.latitud +','+ posicion.longitud +'/'+ Ubicacion[0] +','+ Ubicacion[1]} target="_blank">Abrir Ruta en Google Maps 📍🗺️</a>
            </p>
        </div>
    );
}