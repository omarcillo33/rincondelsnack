import '../../Styles/Home/Tarjetas/estilos.css';

export default function Tarjetas({Descripcion, Url, Alt}){

    return (
        <div className="container-tarjetas">
            <img src={Url} alt={Alt} />
            <p>
                {Descripcion}
            </p>
        </div>
    );

}