
import { useState } from "react";
import '../Styles/NavBar/estilos.css';
import "../Styles/bulma.min.css";
import { Link } from "react-router-dom";


export default function NavBar() {

    const [menuActivo, setMenuActivo] = useState(false);

    
    const abrirMenu = () => {
        setMenuActivo(!menuActivo);
    }

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">

                <Link to={"/"} className="navbar-item">
                        <img src="./img/LogoWeb2.jpg" width="120" height="35" />
                </Link>


                <a role="button" className={"navbar-burger " + (menuActivo ? 'is-active' : '')} aria-label="menu" aria-expanded="false" data-target="navbarBasicExample" onClick={abrirMenu} > 
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" className={"navbar-menu " + (menuActivo ? 'is-active' : '')}>
                <div className="navbar-start">

                    <Link to={"/"} className="navbar-item" onClick={abrirMenu}>
                        Inicio
                    </Link>

                    <Link to={"/Productos"} className="navbar-item" onClick={abrirMenu}>
                        Productos
                    </Link>
                    { /*
                    <div className="navbar-item has-dropdown is-hoverable">
                        <a className="navbar-link">
                            More
                        </a>

                        <div className="navbar-dropdown">
                            <a className="navbar-item">
                                About
                            </a>
                            <a className="navbar-item">
                                Jobs
                            </a>
                            <a className="navbar-item">
                                Contact
                            </a>
                            <hr className="navbar-divider" />
                            <a className="navbar-item">
                                Report an issue
                            </a>
                        </div>
                    </div>
                    */ }   
                </div>
                {/*
                <div className="navbar-end">
                    <div className="navbar-item">
                        <div className="buttons">
                            <a className="button is-primary">
                                <strong>Sign up</strong>
                            </a>
                            <a className="button is-light">
                                Log in
                            </a>
                        </div>
                    </div>
                </div>
                */}
            </div>
        </nav>
    );
}