
export default function ProductoItemMenu({Nombre, Precio, Imagen, Descripcion, Modal}){

    return (
        <div className="itemProductos" onClick={Modal}>
            <div className="imagen">
                <img src={Imagen} alt="Producto" />
            </div>
            <div className="descripcion">
                <div>
                    <b>Producto: </b> <br />
                    <p className='producto-nombre' style={{wordBreak:"keep-all"}}>{Nombre}</p>
                </div>
                <div>
                    <b>Precio: </b> <span className="precio">{Precio}</span>
                </div>
            </div>
        </div>
    );

}