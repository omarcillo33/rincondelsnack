import MyRoutes from "./Routes/MyRoutes";
import "./Styles/general.css"

function App() {
  return(
      <MyRoutes />
  );
}

export default App
