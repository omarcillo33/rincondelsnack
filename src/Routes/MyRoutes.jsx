import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "../Pages/Home";
import Productos from "../Pages/Productos";
import NavBar from "../Components/NavBar";
import { Link } from "react-router-dom";


export default function MyRoutes() {
    return (
        <Router>
            <NavBar />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/Productos" element={<Productos />} />
            </Routes>
        </Router>
    )
}