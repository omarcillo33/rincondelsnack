
import 'leaflet/dist/leaflet.css'
import { MapContainer, TileLayer, useMap , Marker, Popup} from 'react-leaflet'

const TarjetasProps = [
    {
        id: 1,
        descripcion: "Controla el colesterol",
        url: "./img/Colesterol.png",
        alt: "Colesterol"
    },
    {
        id: 2,
        descripcion: "Proporciona energia",
        url: "./img/energia.png",
        alt: "Energia"
    },
    {
        id: 3,
        descripcion: "Funciona como antioxidante",
        url: "./img/Antioxidantes.png",
        alt: "Antioxidante"
    },
    {
        id: 4,
        descripcion: "Fortalece el Cerebro",
        url: "./img/Cerebro.png",
        alt: "Cerebro"
    },
    {
        id: 5,
        descripcion: "Rico en Fibra",
        url: "./img/Fibra.png",
        alt: "Fibra"
    },
    {
        id: 6,
        descripcion: "Previene el Cancer",
        url: "./img/Cancer.png",
        alt: "Cancer"
    }
];

export default function Home() {

    const ubicacion = [23.2206, -106.37021]


    return (
        <div style={{width: "100px"}}>
            <MapContainer center={ubicacion} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    //attribution='&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                    url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={ubicacion}>
                    <Popup>
                        A pretty CSS3 popup. <br /> Easily customizable.
                    </Popup>
                </Marker>
            </MapContainer>
        </div>
    );

}