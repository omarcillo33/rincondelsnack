//import '../Styles/Home/Leafleft.css';
import "../Styles/Home/estilos.css";
import Tarjetas from "../Components/Home/Tarjetas";
import PopupMessage from "../Components/Home/PopupMessage";
import React, { useEffect, useRef, useState } from 'react';
//import { Map, tileLayer, marker } from 'leaflet';

import 'leaflet/dist/leaflet.css'
import { MapContainer, TileLayer, useMap, Marker, Popup } from 'react-leaflet';
import { Icon } from "leaflet";


const TarjetasProps = [
    {
        id: 1,
        descripcion: "Controla el colesterol",
        url: "./img/Colesterol.png",
        alt: "Colesterol"
    },
    {
        id: 2,
        descripcion: "Proporciona energia",
        url: "./img/energia.png",
        alt: "Energia"
    },
    {
        id: 3,
        descripcion: "Funciona como antioxidante",
        url: "./img/Antioxidantes.png",
        alt: "Antioxidante"
    },
    {
        id: 4,
        descripcion: "Fortalece el Cerebro",
        url: "./img/Cerebro.png",
        alt: "Cerebro"
    },
    {
        id: 5,
        descripcion: "Rico en Fibra",
        url: "./img/Fibra.png",
        alt: "Fibra"
    },
    {
        id: 6,
        descripcion: "Previene el Cancer",
        url: "./img/Cancer.png",
        alt: "Cancer"
    }
];

export default function Home() {

    const ubicacion = [23.2206, -106.37021];

    const [estadoModal,setEstadoModal] = useState(true);

    const mapRef = useRef(null)
    const markerRef = useRef(null)

    const Icono = new Icon

    const cerrarModal = () => {
        setEstadoModal(!estadoModal);
    }


    const onClickShowMarker = () => {

        const map = mapRef.current
        if (!map) {
            return
        }

        map.target.flyTo(ubicacion, 15)

        const marker = markerRef.current
        if (marker) {
            marker.openPopup();
            //debugger
            document.querySelector(".leaflet-control-attribution").innerText.replace("© ", "");
        }



    }

    return (
        <div>
            <div className="Banner">
                <img src="./img/Inaguracion.jpg" alt="banner-principal" />
            </div>
            { /*
            <div className="Banner">
                <img src="./img/Banner.jpg" alt="banner-principal" />
            </div>
            
            <div className="galeria">
                <div className="galeria-botonera">
                    °°°°°°°°°°°°°
                </div>
            </div>
            */ }
            <div className="Productos">
                <h1 className="tituloSecciones white" >
                    BENEFICIOS DEL MAIZ
                </h1>
                {
                    TarjetasProps.map(item => {
                        return (
                            <Tarjetas key={item.id} Descripcion={item.descripcion} Url={item.url} Alt={item.alt} />
                        );
                    })
                }

            </div>
            <div className="ubicacionandRedes">
                <div className="Ubicacion">
                    <h1 className="tituloSecciones">
                        UBICACION 📌
                    </h1>
                    <div id="Map" style={{ width: "100%", height: "400px" }}>
                        <MapContainer center={ubicacion} zoom={13} scrollWheelZoom={true}
                            whenReady={(map) => {
                                mapRef.current = map;
                                setTimeout(() => {
                                    onClickShowMarker();
                                }, 500);

                            }}
                        >
                            <TileLayer
                                attribution='&copy; <a href="/">Rincon del Snack MZT</a>'
                                url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"

                            />
                            <Marker ref={markerRef} position={ubicacion}  
                                options={{
                                    icon: {
                                        options: {
                                            iconUrl: "/Localizacion.png" 
                                        }
                                    }
                                }} 
                            >
                                <Popup keepInView={true}>
                                    <PopupMessage Ubicacion={ubicacion} />
                                </Popup>
                            </Marker>
                        </MapContainer>
                    </div>
                </div>
                <div className="Contactanos">
                    <h1 className="tituloSecciones" >
                        CONTACTANOS 📌
                    </h1>
                    <img src="./img/LogoWeb.png" alt="Logo" width={450} />
                    <div className="redSocial-email" style={{ marginTop: "20px" }}>
                        <p><b>Email:</b> rincondelsnack.mzt@gmail.com</p>
                    </div>
                    <div className="redesSociales">
                        <div className="horarios txt-center">
                            <h1>Nuestros Horarios</h1>
                            <div className="w-100 flex flex-wrap">
                                <div className="w-40 txt-right" style={{ paddingRight: "20px" }}>
                                    LUNES:
                                </div>
                                <div className="w-60 txt-left">
                                    4:30 PM - 9:00 PM
                                </div>
                            </div>
                            <div className="w-100 flex flex-wrap">
                                <div className="w-40 txt-right" style={{ paddingRight: "20px" }}>
                                    MARTES:
                                </div>
                                <div className="w-60 txt-left">
                                    4:30 PM - 9:00 PM
                                </div>
                            </div>
                            <div className="w-100 flex flex-wrap">
                                <div className="w-40 txt-right" style={{ paddingRight: "20px" }}>
                                    JUEVES:
                                </div>
                                <div className="w-60 txt-left">
                                    4:30 PM - 9:00 PM
                                </div>
                            </div>
                            <div className="w-100 flex flex-wrap">
                                <div className="w-40 txt-right" style={{ paddingRight: "20px" }}>
                                    VIERNES:
                                </div>
                                <div className="w-60 txt-left">
                                    4:30 PM - 9:00 PM
                                </div>
                            </div>
                        </div>
                        <div className="lista-redes">
                            <ul>
                                <li>
                                    <a className="redSocialIcon" href="https://m.facebook.com/RinconDelSnack.Mzt/" target="_blank">
                                        {/* } <img src="./img/facebookIcono.png" alt="Facebook Red Social" width={30} height={30} /> */}
                                        <svg fill="#2773ec" height="30px" width="30px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" stroke="#2773ec"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M12 2.03998C6.5 2.03998 2 6.52998 2 12.06C2 17.06 5.66 21.21 10.44 21.96V14.96H7.9V12.06H10.44V9.84998C10.44 7.33998 11.93 5.95998 14.22 5.95998C15.31 5.95998 16.45 6.14998 16.45 6.14998V8.61998H15.19C13.95 8.61998 13.56 9.38998 13.56 10.18V12.06H16.34L15.89 14.96H13.56V21.96C15.9164 21.5878 18.0622 20.3855 19.6099 18.57C21.1576 16.7546 22.0054 14.4456 22 12.06C22 6.52998 17.5 2.03998 12 2.03998Z"></path> </g></svg>
                                        Rincon del Snack MZT
                                    </a>
                                </li>
                                <li>

                                    <a className="redSocialIcon" href="https://wa.me/+528143623749" target="_blank">
                                        { /* <img src="./img/facebookIcono.png" alt="Facebook Red Social" width={30} height={30} /> */}
                                        <svg fill="#27d349" height="30px" width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 308 308" xml:space="preserve" stroke="#27d349"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g id="XMLID_468_"> <path id="XMLID_469_" d="M227.904,176.981c-0.6-0.288-23.054-11.345-27.044-12.781c-1.629-0.585-3.374-1.156-5.23-1.156 c-3.032,0-5.579,1.511-7.563,4.479c-2.243,3.334-9.033,11.271-11.131,13.642c-0.274,0.313-0.648,0.687-0.872,0.687 c-0.201,0-3.676-1.431-4.728-1.888c-24.087-10.463-42.37-35.624-44.877-39.867c-0.358-0.61-0.373-0.887-0.376-0.887 c0.088-0.323,0.898-1.135,1.316-1.554c1.223-1.21,2.548-2.805,3.83-4.348c0.607-0.731,1.215-1.463,1.812-2.153 c1.86-2.164,2.688-3.844,3.648-5.79l0.503-1.011c2.344-4.657,0.342-8.587-0.305-9.856c-0.531-1.062-10.012-23.944-11.02-26.348 c-2.424-5.801-5.627-8.502-10.078-8.502c-0.413,0,0,0-1.732,0.073c-2.109,0.089-13.594,1.601-18.672,4.802 c-5.385,3.395-14.495,14.217-14.495,33.249c0,17.129,10.87,33.302,15.537,39.453c0.116,0.155,0.329,0.47,0.638,0.922 c17.873,26.102,40.154,45.446,62.741,54.469c21.745,8.686,32.042,9.69,37.896,9.69c0.001,0,0.001,0,0.001,0 c2.46,0,4.429-0.193,6.166-0.364l1.102-0.105c7.512-0.666,24.02-9.22,27.775-19.655c2.958-8.219,3.738-17.199,1.77-20.458 C233.168,179.508,230.845,178.393,227.904,176.981z"></path> <path id="XMLID_470_" d="M156.734,0C73.318,0,5.454,67.354,5.454,150.143c0,26.777,7.166,52.988,20.741,75.928L0.212,302.716 c-0.484,1.429-0.124,3.009,0.933,4.085C1.908,307.58,2.943,308,4,308c0.405,0,0.813-0.061,1.211-0.188l79.92-25.396 c21.87,11.685,46.588,17.853,71.604,17.853C240.143,300.27,308,232.923,308,150.143C308,67.354,240.143,0,156.734,0z M156.734,268.994c-23.539,0-46.338-6.797-65.936-19.657c-0.659-0.433-1.424-0.655-2.194-0.655c-0.407,0-0.815,0.062-1.212,0.188 l-40.035,12.726l12.924-38.129c0.418-1.234,0.209-2.595-0.561-3.647c-14.924-20.392-22.813-44.485-22.813-69.677 c0-65.543,53.754-118.867,119.826-118.867c66.064,0,119.812,53.324,119.812,118.867 C276.546,215.678,222.799,268.994,156.734,268.994z"></path> </g> </g></svg>
                                        Rincon del Snack MZT (Whats App)
                                    </a>
                                </li>
                                <li>
                                    <a className="redSocialIcon" href="tel:+528143623749" target="_blank">
                                        <svg viewBox="0 0 24 24" height="30px" width="30px" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M18.9998 17.5V6.5C19.0627 5.37366 18.6774 4.2682 17.9279 3.42505C17.1784 2.5819 16.1258 2.06958 14.9998 2H8.99981C7.87387 2.06958 6.82121 2.5819 6.07175 3.42505C5.32228 4.2682 4.9369 5.37366 4.99982 6.5V17.5C4.9369 18.6263 5.32228 19.7317 6.07175 20.5748C6.82121 21.418 7.87387 21.9303 8.99981 21.9999H14.9998C16.1258 21.9303 17.1784 21.418 17.9279 20.5748C18.6774 19.7317 19.0627 18.6263 18.9998 17.5V17.5Z" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path> <path d="M14 5H10" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path> </g></svg>
                                        Llamanos
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class={"modal " + ( estadoModal ? "is-active" : "" )} >
                <div class="modal-background"></div>
                <div class="modal-content">
                    <div className="w-90" style={{ backgroundColor: "white", borderRadius: "20px", padding: "20px" }}>
                        <div className="flex flex-wrap w-100">
                            <p className="w-100 txt-center" style={{fontWeight:"bold", fontSize:"1.6rem", color:"red"}}>
                               PROXIMA GRAN APERTURA
                            </p>
                        </div>
                        <img src="./img/LogoWeb.jpg" alt="Logo" style={{ marginTop: "20px" }} />
                        <div className="flex flex-wrap w-100" style={{ marginTop: "20px" }}>
                            <p className="w-100 txt-center" style={{fontWeight:"bold", fontSize:"1.2rem", color:"black", backgroundColor:"yellow", borderRadius:"20px", padding:"5px"}}>
                            🌽 LO MEJOR DE LOS ELOTES 🌽
                            </p>
                        </div>
                        <div className="flex flex-wrap w-100" style={{ marginTop: "20px" }}>
                            <p className="w-100 txt-center" style={{fontWeight:"bold", fontSize:"1.2rem", color:"black"}}>
                                LUNES 29 DE ENERO 2024
                            </p>
                        </div>
                        <div className="flex flex-wrap w-100" style={{ marginTop: "20px" }}>
                            <p className="w-100 txt-center" style={{color:"blue",fontWeight:"bold", fontSize:"1.2rem"}}>
                                UBICACIÓN:
                            </p>
                            <p className="w-100 txt-center" style={{fontWeight:"bold", fontSize:"1rem", color:"black", marginTop: "10px"}}>
                                CIRCUITO DE LOS MOLINOS NORTE #20603 URBI VILLAS DEL REAL
                            </p>
                        </div>
                    </div>
                </div>
                <button class="modal-close is-large" aria-label="close" onClick={cerrarModal}></button>
            </div>
        </div>
    );

}