import '../Styles/Productos/estilos.css'
import ProductoItemMenu from '../Components/Productos/ProductoItemMenu';
import { useState } from 'react';

const ProductosItemsBasicos = [
    {
        Id: 1,
        Nombre: "Vaso Chico",
        Precio: "$35.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Vaso de Elote preparado al gusto con mayonesa, crema, queso seco."
    },
    {
        Id: 2,
        Nombre: "Vaso Mediano",
        Precio: "$40.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Vaso de Elote preparado al gusto con mayonesa, crema, queso seco."
    },
    {
        Id: 3,
        Nombre: "Medio Litro",
        Precio: "$65.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Vaso de Elote preparado al gusto con mayonesa, crema, queso seco."
    },
    {
        Id: 4,
        Nombre: "Elote Entero Preparado",
        Precio: "$35.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Preparado con mayonesa, crema y queso seco."
    },
    {
        Id: 5,
        Nombre: "Elote Revolcado",
        Precio: "$60.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Preparado con mayonesa, crema, queso amarillo, escarchado de cheetos flaming hot, bañado de queso amarillo y una capa de queso seco."
    },
    {
        Id: 6,
        Nombre: "Tosti Elote",
        Precio: "$70.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Sabrita a elegir, preparado con elote, mayonesa, crema, queso seco y una cubierta de queso amarillo."
    },
];

const ProductosItemsEspeciales = [ 
    {
        Id: 1,
        Nombre: "Volcán",
        Precio: "$100.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Medio litro de elote preparado, una sabrita a elegir, bañado de queso amarillo y seco."
    },
    {
        Id: 2,
        Nombre: "Charola Personal",
        Precio: "$70.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Elote Preparado sobre una base de sabritas a elegir, bañado de queso amarillo y seco."
    },
    {
        Id: 3,
        Nombre: "Charola Grande",
        Precio: "$110.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Elote Preparado sobre una base de sabritas a elegir, bañado de queso amarillo y seco."
    },
    {
        Id: 4,
        Nombre: "Charola DÚO",
        Precio: "$120.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Una Porcion de elote Preparado, 2 mitades de elote entero prepado y una sabrita a elegir."
    },
    {
        Id: 5,
        Nombre: "Charola Compadres",
        Precio: "$180.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "4 Mitades de elotes enteros preparados, 2 sabritas a elegir, 1 porcion de elote preparado en medio, bañado de queso amarillo y seco."
    },
    {
        Id: 6,
        Nombre: "La Greñuda",
        Precio: "N/A",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Una maruchan acompañada de una sabrita a elegir y elote preparado bañado con queso amarillo y seco"
    },
    {
        Id: 7,
        Nombre: "Trozitos Revolcados",
        Precio: "$80",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "8 Trozos (2 Elotes) preparado con mayonesa, crema y queso seco y una capa de queso amarillo"
    },
];

const ProductosItemsOtros = [
    {
        Id: 1,
        Nombre: "Churros Locos",
        Precio: "Ch $50.00  -  Gde $70.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Churritos de Maíz, cacahuates, chacatrozo, pepino, salchicha, cueritos, clamato y limon."
    },
    {
        Id: 2,
        Nombre: "Cueritos Locos",
        Precio: "$60.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Preparados con pepino, tomate, cebolla morada, cacahuate y limon."
    },
];

const ProductosItemsToppings = [
    {
        Id: 1,
        Nombre: "Ingrediente Extra",
        Precio: "$5.00",
        Imagen: "./img/VasoElote.jpg",
        Descripcion: "Queso Seco - Queso Amarillo - Crema - Mayonesa - Cacahuate - Churritos - Chacatrozo"
    },
];

const ProductosItemsBebidas2 = [
    {
        Id: 1,
        Nombre: "Coca Cola 600 ml",
        Precio: "$20.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "Variedad de Sabores (Segun Disponibilidad)"
    },
    {
        Id: 2,
        Nombre: "Tonicol 600 ml",
        Precio: "$20.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "El Sabor Especial de Vainilla"
    },
    {
        Id: 3,
        Nombre: "Agua de Sabor 1 lt",
        Precio: "$20.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "Jamaica, Limon, Cebada, Piña, Tamarindo (Segun Disponibilidad)"
    },
    {
        Id: 4,
        Nombre: "Aguas Natural 1 lt",
        Precio: "$15.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "Agua Purificada (Segun Disponibilidad)"
    },
    {
        Id: 5,
        Nombre: "Te Jaztea 500 ml",
        Precio: "$20.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "Te de Limon de la Marca Jaztea"
    },
];

const ProductosItemsBebidas = [
    {
        Id: 1,
        Nombre: "Te Jaztea 500 ml",
        Precio: "$20.00",
        Imagen: "./img/Bebidas.png",
        Descripcion: "Te de Limon de la Marca Jaztea"
    },
];

export default function Productos() {

    const [itemModal, setItemModal] = useState({
        Id: 0,
        Nombre: "",
        Precio: "",
        Imagen: "",
        Descripcion: ""
    });

    const [mostrarModal, setMostarModal] = useState(false);

    const mostrarDescripcion = (Item) => {
        debugger
        setItemModal({
            Id: Item.Id,
            Nombre: Item.Nombre,
            Precio: Item.Precio,
            Imagen: Item.Imagen,
            Descripcion: Item.Descripcion
        })
        setMostarModal(!mostrarModal);
    }

    const cerrarModal = () => {
        setMostarModal(false);
    }


    return (
        <div className='container'>
            <h1 className='titulo'>MENU</h1>

            <h1 className='subtitulo' >CLASICOS 🌽</h1>
            <div className='containerItems'>
                {
                    ProductosItemsBasicos.map((Item) => {
                        return (
                            <ProductoItemMenu key={Item.Id} Nombre={Item.Nombre} Precio={Item.Precio} Descripcion={Item.Descripcion} Imagen={Item.Imagen} Modal={() => mostrarDescripcion(Item)} />
                        )
                    })
                }
            </div>
            <h1 className='subtitulo'>ESPECIALES ✨</h1>
            <div className='containerItems'>
                {
                    ProductosItemsEspeciales.map((Item) => {
                        return (
                            <ProductoItemMenu key={Item.Id} Nombre={Item.Nombre} Precio={Item.Precio} Descripcion={Item.Descripcion} Imagen={Item.Imagen} Modal={() => mostrarDescripcion(Item)} />
                        )
                    })
                }
            </div>
            <h1 className='subtitulo'>OTROS ✨</h1>
            <div className='containerItems'>
                {
                    ProductosItemsOtros.map((Item) => {
                        return (
                            <ProductoItemMenu key={Item.Id} Nombre={Item.Nombre} Precio={Item.Precio} Descripcion={Item.Descripcion} Imagen={Item.Imagen} Modal={() => mostrarDescripcion(Item)} />
                        )
                    })
                }
            </div>
            <h1 className='subtitulo'>COMPLEMENTOS ✨</h1>
            <div className='containerItems'>
                {
                    ProductosItemsToppings.map((Item) => {
                        return (
                            <ProductoItemMenu key={Item.Id} Nombre={Item.Nombre} Precio={Item.Precio} Descripcion={Item.Descripcion} Imagen={Item.Imagen} Modal={() => mostrarDescripcion(Item)} />
                        )
                    })
                }
            </div>
            <h1 className='subtitulo'>BEBIDAS 🥤</h1>
            <div className='containerItems'>
                {
                    ProductosItemsBebidas.map((Item) => {
                        return (
                            <ProductoItemMenu key={Item.Id} Nombre={Item.Nombre} Precio={Item.Precio} Descripcion={Item.Descripcion} Imagen={Item.Imagen} Modal={() => mostrarDescripcion(Item)} />
                        )
                    })
                }
            </div>
            <div className={"modal " + (mostrarModal ? 'is-active is-clipped' : '')}>
                <div className="modal-background"></div>
                <div className="modal-content">
                    <div className="flex flex-wrap w-90" style={{ justifyContent: "center", background: "white", padding: "20px", borderRadius: "20px" }}>
                        <p className='w-100 txt-center producto-nombre-b' style={{ fontSize: "1.7rem" }}>
                            {itemModal.Nombre}
                        </p>
                        {/* 
                                <img src="./img/VasoElote.jpg" alt="" style={{ width: "40%" ,borderRadius: "100%", backgroundColor: "red"}} />
                                */}
                        <img src={itemModal.Imagen} alt="Imagen Producto" className="w-40" style={{ borderRadius: "100%" }} />
                        <p className='w-100 txt-center' style={{ fontSize: "1.2rem" }}>
                            <b>Precio: </b> <span className='precio'>{itemModal.Precio}</span>
                        </p>
                        {
                            (itemModal.Descripcion != "" ?

                                <p className="w-100 txt-justify" style={{ wordBreak: "keep-all", marginTop: "10px" }}>
                                    <b>Descripcion</b> <br />
                                    {itemModal.Descripcion}
                                </p>

                                :
                                null)
                        }
                    </div>
                </div>
                <button className="modal-close is-large" aria-label="close" onClick={cerrarModal}></button>
            </div>
        </div>
    );

}